import pandas as pd
import os


def split_file(file_path, output_folder, rows_per_file=249):
    _, file_extension = os.path.splitext(file_path)

    if file_extension.lower() == '.xlsx':
        split_excel_file(file_path, output_folder, rows_per_file)
    elif file_extension.lower() == '.csv':
        split_csv_file(file_path, output_folder, rows_per_file)
    else:
        print(f"Unsupported file format: {file_extension}")


def split_excel_file(file_path, output_folder, rows_per_file=249):
    df = pd.read_excel(file_path)
    rows_per_file = min(rows_per_file, len(df))
    smaller_dfs = [df[i:i+rows_per_file]
                   for i in range(0, len(df), rows_per_file)]

    for i, smaller_df in enumerate(smaller_dfs):
        output_file = f'{output_folder}/bblp_query_file_{i+1}.xlsx'
        smaller_df.to_excel(output_file, index=False)

    print(
        f'Successfully split the Excel file into {len(smaller_dfs)} smaller files.')


def split_csv_file(file_path, output_folder, rows_per_file=249):
    df = pd.read_csv(file_path)
    rows_per_file = min(rows_per_file, len(df))
    smaller_dfs = [df[i:i+rows_per_file]
                   for i in range(0, len(df), rows_per_file)]

    for i, smaller_df in enumerate(smaller_dfs):
        output_file = f'{output_folder}/bblp_query_file_{i+1}.csv'
        smaller_df.to_csv(output_file, index=False)

    print(
        f'Successfully split the CSV file into {len(smaller_dfs)} smaller files.')
