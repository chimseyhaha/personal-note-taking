
## IP


## Gateway
Gateway = default gateway (ip address) or router
- is a networking device that connects different networks together.

For example, if your router's IP address is 192.168.1.1, when your device wants to access a website, it will send its request to that IP address. The router then uses its own internal routing capabilities to direct the data to the right destination and handles the return data similarly.


## NAT **(Network Address Translation)**
- NAT is primarily used to translate between private and public IP addresses to enable multiple devices to share a single public IP address

- When devices on the local network send data packets to the internet, the router performs the translation. When responses come back from the internet, the router uses the translation table to route the response packets to the correct device within the local network.

- NAT provides an additional layer of security by not exposing individual device IP addresses to the external network.

## Proxy

- Proxy server acts as an intermediary between clients (like your computer) and servers  (like a website), often providing features like caching, content filtering, and anonymity.