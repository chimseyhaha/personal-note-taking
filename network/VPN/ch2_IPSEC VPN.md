- 영어
https://www.youtube.com/watch?v=15amNny_kKI

- 한국 YT
https://www.youtube.com/watch?v=2Qc_aeRl1HI
-
## 1. IPsec

IPSec 
- Secure communication over IP networks.
- Ensuring the confidentiality, integrity, and authenticity of data transmitted between devices on a network
- IPsec operates at the network layer (Layer 3) of the OSI model.

IPsec consists of two main components:
1. **Authentication Header (AH):**
2. **Encapsulating Security Payload (ESP):**

IPsec can operate in two main modes:
1. **Transport Mode**
2. **Tunnel Mode:**

IPsec can be used for various purposes, including:
- **VPN Creation:** IPsec is often used to create secure tunnels between networks or devices over the internet, creating a virtual private network.
- **Remote Access:** It provides a secure way for remote users to connect to a corporate network or other resources.
- **Site-to-Site Communication:** IPsec can secure communication between different office locations of an organization.
- **Data Protection:** It ensures the confidentiality and integrity of sensitive data transmitted over public networks.


## 2. VPN IPSec

- IPSEC is a group of protocols
- It set up secure tunnels across insecure network


- IPSEC has 2 main phases
- IKE phase 1(Slow &Heavy)


IKE SA Create (Phase 1 tunnel)

- IKE potocol (Internet Key Exchange) 

IKE Phase 2 (Fast & Agile)


---
VDI = Virtual Desktop Infrastructure 

Route Vs Policy Based VPN

Route-based = single SA Pair = single IPSEC Key


## 3. Setting up an IPsec VPN

- Components
	- **VPN Gateway/Firewall Device:*
	- **Remote Access Clients**
	- **Network Infrastructure**: routers, switches, and other networking equipment.

**Steps to Set Up an IPsec VPN:**

1. **Selecting the VPN Gateway Device:** Choose a VPN gateway/firewall device that supports IPsec VPN functionality. This could be a dedicated hardware appliance or a software-based solution that runs on a server or a virtual machine.
    
2. **Planning the Network Topology:** Decide on the type of IPsec VPN you want to set up. Will it be a remote access VPN for individual users, or a site-to-site VPN connecting two networks? Plan the IP addressing, subnets, and routing accordingly.
    
3. **Configuring the VPN Gateway:**
    - Configure the VPN gateway with the necessary IPsec settings. This includes selecting **encryption algorithms, authentication methods, and key exchange protocols.**
    - Set up **security associations (SAs)** for the VPN connections. Define which traffic should be encrypted and authenticated.
    - Configure the VPN client authentication method. This could involve using pre-shared keys, digital certificates, or other methods.
4. **Setting Up VPN Clients:**
    - Install VPN client software on remote access devices.
    - Configure the VPN client with the necessary connection details, including the VPN gateway's IP address, authentication method, and shared secrets/certificates.
5. **Configuring Network Devices:**
    - If you're setting up a site-to-site VPN, configure the network devices (routers, firewalls) at both ends to allow traffic to flow between the connected networks.
    - Ensure that the necessary routes are set up to direct traffic through the VPN tunnel.
6. **Testing and Troubleshooting:**
    
    - Test the VPN connection by connecting remote access clients or sending traffic between the connected networks.
    - Monitor logs and perform troubleshooting if there are connectivity issues.
7. **Security Considerations:**
    
    - Regularly update and patch the VPN gateway device to address security vulnerabilities.
    - Implement strong authentication methods and encryption algorithms to ensure the security of the VPN traffic.



## 4. DNS Configuration

**VPN and DNS Configuration:**

1. **DNS Leak:** When you connect to a VPN, your internet traffic is encrypted and routed through the VPN server. However, DNS queries (resolving domain names) can sometimes bypass the VPN tunnel, potentially revealing your browsing activity to your ISP or other third parties. This is known as a DNS leak.
    
2. **VPN DNS Server:** Many VPN providers offer their own DNS servers that you can use. These DNS servers ensure that your DNS queries are encrypted and routed through the VPN tunnel, preventing DNS leaks.
    
3. **Configuring DNS for VPN:**
    
    - Some VPN clients automatically configure your DNS settings when you connect to their servers.
    - Others might require manual configuration to use the VPN's DNS servers or specify that your existing DNS settings should be overridden while the VPN is active.

**Importance of Proper DNS Configuration with VPN:**

1. **Privacy:** Configuring your DNS settings to use the VPN's DNS servers ensures that all your online activities, including DNS queries, remain private and secure within the encrypted VPN tunnel.
    
2. **Circumventing Geo-restrictions:** VPNs are often used to access regionally restricted content. By using the VPN's DNS servers, you can access websites as if you're in a different location, enhancing your online freedom.
    
3. **Preventing Tracking:** Without proper DNS configuration, websites and online services might still be able to track your browsing habits even when you're using a VPN.

---
### Keywords
- Encryption, Decryption, 
- Symmetric Encryption: Same key is used for both encryption and decryption. Both parties communicating need to share the secret key in advance. Ex: AES and DES.
	- Encryption is fast, but it's a challenge to exchange key securely.
- Asymmetric Encryption:  Uses a pair of keys: a public key for encryption and a private key for decryption.
	- Asymmetric Encryption is slow, but you can easily exchange public keys.
- Hashing, digital Signatures, Key exchange, Authentication
- Cryptographic technique = coverting to encrypted form. can deciphered by authorized individuals (key).
